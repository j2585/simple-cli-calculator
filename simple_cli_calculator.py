# Version 1.1

from sys import argv
from math import sqrt


def sanity_check(args):
    if ((len(args) % 2) == 0 and len(args) >= 2) or len(args) == 0:
        print(f"Error: Insufficient number of arguments.\nNumber of arguments given: {len(args)}")
        exit(1)
    operators = ['+', '-', '*', '/', '^']
    if any(ops in args for ops in operators): return
    while any('sqrt' in arguments for arguments in args): return
    print("Error: No operators given."); exit (1)


def squareroot(args):
    while any('sqrt' in arguments for arguments in args):
        for index, each in enumerate(args):
            if args[index] == 'sqrt':
                print("Error: Did not specify squareroot value.")
                exit(1)
            if 'sqrt' not in args[index]: continue
            args[index] = args[index].replace('sqrt', '')
            if float(args[index]) < 0:
                print("Error: Not able to calculate square root of a negative.")
                exit(1)
            args[index] = str(sqrt(float(args[index])))
    return args


def replace_comma(args):
    for index, each in enumerate(args):
        args[index] = args[index].replace(",", ".")
        if args[index].count('.') > 1:
            print("Error! Multiple commas.")
            exit(1)
    return args


def replace_minus(args):
    while '-' in args:
        for index, each in enumerate(args):
            if each != '-':
                continue
            args[index + 1] = f"-{args[index + 1]}" if float(args[index + 1]) > 0 else args[index + 1].replace('-', '')
            args[index] = "+"
    return args


def calc_function(symbol, args):
    op = {
        '+': lambda x, y: x + y,
        '*': lambda x, y: x * y,
        '/': lambda x, y: x / y if y != 0 else (print(f"Error! Zero division!"), exit(1)),
        '^': lambda x, y: x ** y
    }
    while symbol in args:
        for index, each in enumerate(args):
            if each != symbol:
                continue
            args[index] = op[symbol](float(args[index - 1]), float(args[index + 1]))
            args.pop(index - 1)
            args.pop(index)
    return args


def main(*args):
    arguments = argv
    arguments.pop(0)
    try:
        sanity_check(arguments)
        arguments = replace_comma(arguments)
        arguments = squareroot(arguments)
        if '-' in arguments:
            arguments = replace_minus(arguments)
        if '^' in arguments:
            arguments = calc_function('^', arguments)
        if '/' in arguments:
            arguments = calc_function('/', arguments)
        if '*' in arguments:
            arguments = calc_function('*', arguments)
        if '+' in arguments:
            arguments = calc_function('+', arguments)
        return f"{int(float(arguments[0])) if float(arguments[0]) == int(float(arguments[0])) else arguments[0]}"
    except BaseException as err:
        print(f"An error occured!\nUnexpected {err = }, {type(err) = }")
        exit(1)


if __name__ == '__main__':
    print(f"{main()}")
