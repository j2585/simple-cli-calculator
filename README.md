This is a very simple command line calculator written in Python. It can add, subtract, multiply, divide, factor and get the square root, all while keeping the order of operations.
If you need a quick and easy to use calculator for the terminal for small, simple and fast calculations and without requiring you to start an extra program interface like GNU's "bc" (which I would recommend to you for more complex operations), this is adequate to do the job. One line, one string of operations, one output. 

Now right out of the box bash will use the build in wildcard characters to replace asterisks with the files in your directory.
To circumvent this problem and get a functioning calculator, add the following line to your .bashrc file as an alias:

```bash
alias calc='set -f; calculator'; calculator(){ command python /path/to/the/simple-cli-calculator.py "$@"; set +f;}
```
This will temporarily disable the wildcard asterisk.

For zsh users, add the following to your .zshrc:

```zsh
function function calculator {
	command python /path/to/the/simple-cli-calculator.py "$@";
}
alias calc='noglob calculator'
```

Usage example:
```
$~: calc 15 + 6 - sqrt144 * 2
$~: -3
```
